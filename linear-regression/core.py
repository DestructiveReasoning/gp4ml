import numpy as np
import matplotlib.pyplot as plt

def generate_dataset(n_points, x_range, fn, var=0.5):
    xs = np.random.uniform(low=x_range[0], high=x_range[1], size=(n_points,))
    ys = fn(xs) + np.random.normal(0, var, size=(n_points,))
    return xs, ys

def plot_with_error_regions(xs, regressor, projector=lambda x: x, color='blue', error_alpha=0.3):
    sample_means = []
    sample_vars = []
    for x in xs:
        phi = projector(x)
        sample_mean, sample_var = regressor.prediction_params(phi)
        sample_means.append(sample_mean)
        sample_vars.append(sample_var)

    sample_means = np.array(sample_means)
    sample_vars = np.array(sample_vars)

    plt.plot(xs, sample_means, color)
    plt.fill_between(xs, sample_means - sample_vars, sample_means + sample_vars, color=color, alpha=error_alpha)
